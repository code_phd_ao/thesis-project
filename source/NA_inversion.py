"""
Written by Mathieu Le Feuvre in November 2015
Neighborhood Algorithm, after Sambridge (1999a), modified by Wathelet (2006)

12/06/2020
add function TLI_vph_inversion, time-lapse inversion using vph as data (instead of diagram)
"""
from __future__ import division
from numpy import *
#from forward import mat_disperse # Forward Problem
from math_func import dvph_df, gpdc_calculation
from math_func import histogram_distance, diagram_v_f, seismo_tx_filter, diagram_distance #realiste_error,
import forward
import matplotlib.pyplot as plt
#%%
def vph_inversion(d0,err,mmin,mmax,ni,n0,n1,n2,dns,cvp,freq,realist_err=0,opt="phase"):

    #Input 
    # d0 : vector of observed data
    # err : vector of data error bars
    # mmin : vector of minimum allowed values for model parameters
    # mmax : vector of maximum allowed values for model parameters
    # ni : number of iterations
    # n0 : number of initial random model samples
    # n1 : number of best models used at each iteration
    # n2 : number of new models in the neighborhood of previous best models
    # realist_err : realistic error to select models
    
    #Output 
    #mo : array of all tested model parameters (sorted by ascending probability P = exp(-misfit))
    #Pmo : vector of model probability, sorted by ascending probability
    #pchrono : vector of model probability, chronologically stored 
    layer = int((len(mmin)+1)/2)
    order=2; #Order of the misffit (2 = Gaussian errors & Least-Square)

    [nm]=mmin.shape #number of parameters 
    nmo=n0+ni*n1*n2    #Total number of models

    mo=zeros((nmo,nm)) #tested models 
    Pmo=zeros(nmo) #model probability = exp(-misfit)
    pchrono=zeros(nmo) #Chronological evolution of the probability
    m_err = zeros((nmo,nm))
    P_err = zeros(nmo)
    #Scaling factors
    Ascal =  mmax - mmin 
    Bscal = mmin

    #n0 intitial random models 
    mo[0:n0,0:nm]=random.rand(n0,nm)
    for l in range(n0): # loop on initial random models
        
        mscal = mo[l,:]*Ascal+Bscal 
        #print(mscal)
        d = 1/(gpdc_calculation(freq, cvp, mscal[0:layer], dns, append(mscal[layer:], 0.0), 1, opt)[:,1])
#        d = mat_disperse([mscal[2]], dns, cvp, mscal[0:2], freq, opt)[:,0]
#        print(d.shape, d0.shape, err.shape)
        misfit=sqrt(mean((abs(d-d0)/err)**order))
        Pmo[l]=exp(-misfit)
#        compare = greater(realist_err, abs(d-d0))   # compare difference with giving error
#        m_err[l,:] = mscal*(sum(compare)==len(freq))
#        P_err[l] = exp(-misfit)*(sum(compare)==len(freq))
       
    # max and min values in random models    
    minlim1=amin(mo[0:n0,:],axis=0)
    maxlim2=amax(mo[0:n0,:],axis=0)

    nmo=n0

    pchrono[0:nmo]=Pmo[0:nmo]

    #Sorting models according to increasing probability
    temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
    temp = temp[temp[:,0].argsort(),]

    Pmo[0:nmo] = temp[0:nmo,0]
    mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    box1=zeros(nm)
    box2=zeros(nm)

    for i in range(ni) : # loop on number of iterations

        nbcell=nmo # number of cells
        #z2=zeros(nbcell)
        x=zeros(nbcell)
    
        #Boundary values for n1 best parameters 
        box1=amin(mo[nmo-n1:nmo,0:nm],axis=0) # nm: number of parameters
        box2=amax(mo[nmo-n1:nmo,0:nm],axis=0)

        #Descaling
        mo=mo*Ascal+Bscal
        minlim1=minlim1*Ascal+Bscal
        maxlim2=maxlim2*Ascal+Bscal
        box1=box1*Ascal+Bscal
        box2=box2*Ascal+Bscal
    
        #New scale
        Ascal=box2-box1
        Bscal=box1    
        Ascal[Ascal==0]=1
    
        minlim1=(minlim1-Bscal)/Ascal
        maxlim2=(maxlim2-Bscal)/Ascal
   
        #Rescaling 
        mo=(mo-Bscal)/Ascal

        #In the n1 best cells
        for icell in range(nbcell-n1,nbcell) :  

            z2=0
        
            for k in range(1,nm) : # loop on number of parameters
                z2=z2+(mo[0:nbcell,k]-mo[icell,k])**2
        
            #current model
            mocur=copy(mo[icell,:])

            #Sampling n2 models in each n1 best cell
            for j in range(n2) : # loop on number of new models
                
                for k in range(nm) : # loop on number of parameters
                    
                    #computation of distances
                    x[icell]=100*maxlim2[k] #Large arbitrary number for the cell we are in
                    index=arange(nbcell)
                    index=index[index!=icell] #all other cells than icell
                    x[index]=1/2*(mo[icell,k]+mo[index,k]+(z2[icell]-z2[index])/(mo[icell,k]-mo[index,k]))

                    if x[x<=mocur[k]].shape[0]==0 : #cell on the edge
                        l1=minlim1[k]
                    else : #cell not on the edge
                        l1=amax(x[x<=mocur[k]])

                    if x[x>=mocur[k]].shape[0]==0 : #cell on the edge
                        l2=maxlim2[k]
                    else : #cell not on the edge
                        l2=amin(x[x>=mocur[k]])
                
                    #Probably not necessary :
                    if l1<minlim1[k] :
                        l1=minlim1[k]
                    if l2>maxlim2[k] :
                        l2=maxlim2[k]
                
                    #New sampled model : random walk from previous model
                    mo[nmo,:]=copy(mocur)
                    mo[nmo,k]=random.random()*(l2-l1)+l1
                
                    if k==nm-1 :
                        knew=0
                    else :
                        knew=k+1
                    #New computation of distances
                    z2=z2+(mo[0:nbcell,k]-mo[nmo,k])**2-(mo[0:nbcell,knew]-mo[nmo,knew])**2
                
                    mocur=copy(mo[nmo,:])

                mscal=mo[nmo,:]*Ascal+Bscal
                #print(mscal)
                d = 1/(gpdc_calculation(freq, cvp, mscal[0:layer], dns, append(mscal[layer:], 0.0), 1, opt)[:,1])
                #print(mscal)
#                d = mat_disperse([mscal[2]], dns, cvp, mscal[0:2], freq, opt)[:,0]
                misfit=sqrt(mean((abs(d-d0)/err)**order))
                Pmo[nmo]=exp(-misfit)
#                compare = greater(realist_err, abs(d-d0))   # compare difference with giving error
#                m_err[nmo,:] = mscal*(sum(compare)==len(freq))
#                P_err[nmo] = exp(-misfit)*(sum(compare)==len(freq))
                    
                pchrono[nmo]=Pmo[nmo]
                nmo=nmo+1
        #Sorting
        temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
        temp = temp[temp[:,0].argsort(),]

        Pmo[0:nmo] = temp[0:nmo,0]
        mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    for k in range(nm) :  
        mo[:,k]=mo[:,k]*Ascal[k]+Bscal[k] 

    return mo,Pmo,pchrono#,m_err,P_err


#%% 

def dvph_df_inversion(d0,err,mmin,mmax,ni,n0,n1,n2,dns,cvp,freq):#,realist_err=0,vph_ref=0,opt = 1):

    #Input 
    # d0 : vector of observed data
    # err : vector of data error bars
    # mmin : vector of minimum allowed values for model parameters
    # mmax : vector of maximum allowed values for model parameters
    # ni : number of iterations
    # n0 : number of initial random model samples
    # n1 : number of best models used at each iteration
    # n2 : number of new models in the neighborhood of previous best models
    # inv_opt : option for inversion : 1 for Vph inversion ; 2 for dVph/df inversion
    # realist_err : realistic error to select models
    
    #Output 
    #mo : array of all tested model parameters (sorted by ascending probability P = exp(-misfit))
    #Pmo : vector of model probability, sorted by ascending probability
    #pchrono : vector of model probability, chronologically stored 
    layer = int((len(mmin)+1)/2)
    d0 = d0[:]-1 # water lever to avoid instability at HF
#    err = d0.copy()
    err=1
    order=2; #Order of the misffit (2 = Gaussian errors & Least-Square)

    [nm]=mmin.shape #number of parameters 
    nmo=n0+ni*n1*n2    #Total number of models

    mo=zeros((nmo,nm)) #tested models 
    Pmo=zeros(nmo) #model probability = exp(-misfit)
    pchrono=zeros(nmo) #Chronological evolution of the probability
    m_err = zeros((nmo,nm))
    P_err = zeros(nmo)
    #Scaling factors
    Ascal =  mmax - mmin 
    Bscal = mmin

    #n0 intitial random models 
    mo[0:n0,0:nm]=random.rand(n0,nm)
    for l in range(n0):
        mscal = mo[l,:]*Ascal+Bscal 
        vph = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns, append(mscal[layer:], 0.0))[1:-1,1])
        vg = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns, append(mscal[layer:], 0.0), 1, 'group')[:,1])
        d = vph*(vg-vph)/(freq[1:-1]*vg)-1
        misfit=sqrt(mean((abs(d-d0)/err)**order))
        Pmo[l]=exp(-misfit)
#        compare = greater(realist_err, abs(vph-vph_ref))   # compare difference with giving error
#        m_err[l,:] = mscal*(sum(compare)==len(compare))
#        P_err[l] = exp(-misfit)*(sum(compare)==len(compare))
    
        
    minlim1=amin(mo[0:n0,:],axis=0)
    maxlim2=amax(mo[0:n0,:],axis=0)

    nmo=n0

    pchrono[0:nmo]=Pmo[0:nmo]

    #Sorting models according to increasing probability
    temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
    temp = temp[temp[:,0].argsort(),]

    Pmo[0:nmo] = temp[0:nmo,0]
    mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    box1=zeros(nm)
    box2=zeros(nm)

    for i in range(ni) :

        nbcell=nmo
        #z2=zeros(nbcell)
        x=zeros(nbcell)
    
        #Boundary values for n1 best parameters 
        box1=amin(mo[nmo-n1:nmo,0:nm],axis=0)
        box2=amax(mo[nmo-n1:nmo,0:nm],axis=0)

        #Descaling
        mo=mo*Ascal+Bscal
        minlim1=minlim1*Ascal+Bscal
        maxlim2=maxlim2*Ascal+Bscal
        box1=box1*Ascal+Bscal
        box2=box2*Ascal+Bscal
    
        #New scale
        Ascal=box2-box1
        Bscal=box1    
        Ascal[Ascal==0]=1
    
        minlim1=(minlim1-Bscal)/Ascal
        maxlim2=(maxlim2-Bscal)/Ascal
   
        #Rescaling 
        mo=(mo-Bscal)/Ascal

        #In the n1 best cells
        for icell in range(nbcell-n1,nbcell) :  

            z2=0
        
            for k in range(1,nm) :
                z2=z2+(mo[0:nbcell,k]-mo[icell,k])**2
        
            #current model
            mocur=copy(mo[icell,:])

            #Sampling n2 models in each n1 best cell
            for j in range(n2) :
                
                for k in range(nm) :
                    
                    #computation of distances
                    x[icell]=100*maxlim2[k] #Large arbitrary number for the cell we are in
                    index=arange(nbcell)
                    index=index[index!=icell] #all other cells than icell
                    x[index]=1/2*(mo[icell,k]+mo[index,k]+(z2[icell]-z2[index])/(mo[icell,k]-mo[index,k]))

                    if x[x<=mocur[k]].shape[0]==0 : #cell on the edge
                        l1=minlim1[k]
                    else : #cell not on the edge
                        l1=amax(x[x<=mocur[k]])

                    if x[x>=mocur[k]].shape[0]==0 : #cell on the edge
                        l2=maxlim2[k]
                    else : #cell not on the edge
                        l2=amin(x[x>=mocur[k]])
                
                    #Probably not necessary :
                    if l1<minlim1[k] :
                        l1=minlim1[k]
                    if l2>maxlim2[k] :
                        l2=maxlim2[k]
                
                    #New sampled model : random walk from previous model
                    mo[nmo,:]=copy(mocur)
                    mo[nmo,k]=random.random()*(l2-l1)+l1
                
                    if k==nm-1 :
                        knew=0
                    else :
                        knew=k+1
                    #New computation of distances
                    z2=z2+(mo[0:nbcell,k]-mo[nmo,k])**2-(mo[0:nbcell,knew]-mo[nmo,knew])**2
                
                    mocur=copy(mo[nmo,:])

                mscal=mo[nmo,:]*Ascal+Bscal
                vph = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns,append(mscal[layer:], 0.0))[1:-1,1])
                vg = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns, append(mscal[layer:], 0.0), 1, 'group')[:,1])
                d = vph*(vg-vph)/(freq[1:-1]*vg)-1
#                toto = mat_disperse([mscal[2]], dns, cvp, mscal[0:2], freq, opt)[:,0]
#                d = dvph_df(toto, freq)
                misfit=sqrt(mean((abs(d-d0)/err)**order))
                Pmo[nmo]=exp(-misfit)
#                compare = greater(realist_err, abs(vph-vph_ref))   # compare difference with giving error
#                m_err[nmo,:] = mscal*(sum(compare)==len(compare))
#                P_err[nmo] = exp(-misfit)*(sum(compare)==len(compare))
                
                    
                pchrono[nmo]=Pmo[nmo]
                nmo=nmo+1
        #Sorting
        temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
        temp = temp[temp[:,0].argsort(),]

        Pmo[0:nmo] = temp[0:nmo,0]
        mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    for k in range(nm) :  
        mo[:,k]=mo[:,k]*Ascal[k]+Bscal[k] 

    return mo,Pmo,pchrono#, m_err, P_err

#%%
    
def complex_inversion(d0,err,mmin,mmax,ni,n0,n1,n2,dns,cvp,freq,f1, f2):#,realist_err=0,vph_ref=0,opt = 1):

    #Input 
    # d0 : vector of observed data
    # err : vector of data error bars
    # mmin : vector of minimum allowed values for model parameters
    # mmax : vector of maximum allowed values for model parameters
    # ni : number of iterations
    # n0 : number of initial random model samples
    # n1 : number of best models used at each iteration
    # n2 : number of new models in the neighborhood of previous best models
    # inv_opt : option for inversion : 1 for Vph inversion ; 2 for dVph/df inversion
    # realist_err : realistic error to select models
    
    #Output 
    #mo : array of all tested model parameters (sorted by ascending probability P = exp(-misfit))
    #Pmo : vector of model probability, sorted by ascending probability
    #pchrono : vector of model probability, chronologically stored 
    layer = int((len(mmin)+1)/2)
    d0 = d0[:] # water lever to avoid instability at HF
    err = d0.copy()
    order=2; #Order of the misffit (2 = Gaussian errors & Least-Square)
    d = zeros([len(d0)])
    
    [nm]=mmin.shape #number of parameters 
    nmo=n0+ni*n1*n2    #Total number of models

    mo=zeros((nmo,nm)) #tested models 
    Pmo=zeros(nmo) #model probability = exp(-misfit)
    pchrono=zeros(nmo) #Chronological evolution of the probability
    m_err = zeros((nmo,nm))
    P_err = zeros(nmo)
    #Scaling factors
    Ascal =  mmax - mmin 
    Bscal = mmin

    #n0 intitial random models 
    mo[0:n0,0:nm]=random.rand(n0,nm)
    for l in range(n0):
        
        mscal = mo[l,:]*Ascal+Bscal 
        vph = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns, append(mscal[layer:], 0.0))[1:-1,1])
        vg = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns, append(mscal[layer:], 0.0), 1, 'group')[:,1])
        d = vph*(vg-vph)/(freq[1:-1]*vg)
        d[freq[1:-1]<=f1] = vph[freq[1:-1]<=f1]
        d[freq[1:-1]>=f2] = vph[freq[1:-1]>=f2]
        misfit=sqrt(mean((abs(d-d0)/err)**order))
        Pmo[l]=exp(-misfit)
#        compare = greater(realist_err, abs(vph-vph_ref))   # compare difference with giving error
#        m_err[l,:] = mscal*(sum(compare)==len(compare))
#        P_err[l] = exp(-misfit)*(sum(compare)==len(compare))
    
        
    minlim1=amin(mo[0:n0,:],axis=0)
    maxlim2=amax(mo[0:n0,:],axis=0)

    nmo=n0

    pchrono[0:nmo]=Pmo[0:nmo]

    #Sorting models according to increasing probability
    temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
    temp = temp[temp[:,0].argsort(),]

    Pmo[0:nmo] = temp[0:nmo,0]
    mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    box1=zeros(nm)
    box2=zeros(nm)

    for i in range(ni) :

        nbcell=nmo
        #z2=zeros(nbcell)
        x=zeros(nbcell)
    
        #Boundary values for n1 best parameters 
        box1=amin(mo[nmo-n1:nmo,0:nm],axis=0)
        box2=amax(mo[nmo-n1:nmo,0:nm],axis=0)

        #Descaling
        mo=mo*Ascal+Bscal
        minlim1=minlim1*Ascal+Bscal
        maxlim2=maxlim2*Ascal+Bscal
        box1=box1*Ascal+Bscal
        box2=box2*Ascal+Bscal
    
        #New scale
        Ascal=box2-box1
        Bscal=box1    
        Ascal[Ascal==0]=1
    
        minlim1=(minlim1-Bscal)/Ascal
        maxlim2=(maxlim2-Bscal)/Ascal
   
        #Rescaling 
        mo=(mo-Bscal)/Ascal

        #In the n1 best cells
        for icell in range(nbcell-n1,nbcell) :  

            z2=0
        
            for k in range(1,nm) :
                z2=z2+(mo[0:nbcell,k]-mo[icell,k])**2
        
            #current model
            mocur=copy(mo[icell,:])

            #Sampling n2 models in each n1 best cell
            for j in range(n2) :
                
                for k in range(nm) :
                    
                    #computation of distances
                    x[icell]=100*maxlim2[k] #Large arbitrary number for the cell we are in
                    index=arange(nbcell)
                    index=index[index!=icell] #all other cells than icell
                    x[index]=1/2*(mo[icell,k]+mo[index,k]+(z2[icell]-z2[index])/(mo[icell,k]-mo[index,k]))

                    if x[x<=mocur[k]].shape[0]==0 : #cell on the edge
                        l1=minlim1[k]
                    else : #cell not on the edge
                        l1=amax(x[x<=mocur[k]])

                    if x[x>=mocur[k]].shape[0]==0 : #cell on the edge
                        l2=maxlim2[k]
                    else : #cell not on the edge
                        l2=amin(x[x>=mocur[k]])
                
                    #Probably not necessary :
                    if l1<minlim1[k] :
                        l1=minlim1[k]
                    if l2>maxlim2[k] :
                        l2=maxlim2[k]
                
                    #New sampled model : random walk from previous model
                    mo[nmo,:]=copy(mocur)
                    mo[nmo,k]=random.random()*(l2-l1)+l1
                
                    if k==nm-1 :
                        knew=0
                    else :
                        knew=k+1
                    #New computation of distances
                    z2=z2+(mo[0:nbcell,k]-mo[nmo,k])**2-(mo[0:nbcell,knew]-mo[nmo,knew])**2
                
                    mocur=copy(mo[nmo,:])

                mscal=mo[nmo,:]*Ascal+Bscal
                vph = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns, append(mscal[layer:], 0.0))[1:-1,1])
                vg = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns, append(mscal[layer:], 0.0), 1, 'group')[:,1])
                d = vph*(vg-vph)/(freq[1:-1]*vg)
                d[freq[1:-1]<=f1] = vph[freq[1:-1]<=f1]
                d[freq[1:-1]>=f2] = vph[freq[1:-1]>=f2]
#                toto = mat_disperse([mscal[2]], dns, cvp, mscal[0:2], freq, opt)[:,0]
#                d = dvph_df(toto, freq)
                misfit=sqrt(mean((abs(d-d0)/err)**order))
                Pmo[nmo]=exp(-misfit)
#                compare = greater(realist_err, abs(vph-vph_ref))   # compare difference with giving error
#                m_err[nmo,:] = mscal*(sum(compare)==len(compare))
#                P_err[nmo] = exp(-misfit)*(sum(compare)==len(compare))
                
                    
                pchrono[nmo]=Pmo[nmo]
                nmo=nmo+1
        #Sorting
        temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
        temp = temp[temp[:,0].argsort(),]

        Pmo[0:nmo] = temp[0:nmo,0]
        mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    for k in range(nm) :  
        mo[:,k]=mo[:,k]*Ascal[k]+Bscal[k] 

    return mo,Pmo,pchrono#, m_err, P_err
#%%
def diagram_inversion(d0,S_vf0,signal,t,offset,v,mmin,mmax,ni,n0,n1,n2,dns,cvp,fmin,fmax,distance_type):#,realist_err=0,vph_ref=0,opt = 1):

    #Input 
    # d0 : vector of observed data distance
    # err1 : error of baseline data
    # err2 : error of repeatline data
    # mmin : vector of minimum allowed values for model parameters
    # mmax : vector of maximum allowed values for model parameters
    # ni : number of iterations
    # n0 : number of initial random model samples
    # n1 : number of best models used at each iteration
    # n2 : number of new models in the neighborhood of previous best models
    # cvs0,h0 : inversed model of baseline
    # freq : frequency vector with a certain given range
    
    
    #Output 
    #mo : array of all tested model parameters (sorted by ascending probability P = exp(-misfit))
    #Pmo : vector of model probability, sorted by ascending probability
    #pchrono : vector of model probability, chronologically stored 
    
    order=2; #Order of the misffit (2 = Gaussian errors & Least-Square)
    d = zeros([len(d0)])
    
    # build the frequency vector 
    nt = len(t)
    dt = t[1]-t[0]
    fftspace = fft.rfftfreq(nt, dt)
    finv = fftspace[(fftspace>=fmin)&(fftspace<=fmax)]
    
    # velocity information
    vmin, vmax, nv = min(v), max(v), len(v)
    
    [nm]=mmin.shape #number of parameters 
    nmo=n0+ni*n1*n2    #Total number of models

    mo=zeros((nmo,nm)) #tested models 
    Pmo=zeros(nmo) #model probability = exp(-misfit)
    pchrono=zeros(nmo) #Chronological evolution of the probability
    #Scaling factors
    Ascal =  mmax - mmin 
    Bscal = mmin

    #n0 intitial random models 
    mo[0:n0,0:nm]=random.rand(n0,nm)
    for l in range(n0):
        
        mscal = mo[l,:]*Ascal+Bscal 
        vph = 1/(gpdc_calculation(finv, cvp, mscal[0:2], dns, append(mscal[2], 0.0))[:,1])
        s_tx = seismo_tx_filter(t, offset, signal, vph, fmin, fmax)
        _, _, S_vf = diagram_v_f(s_tx, offset, t, fmin, fmax, vmax, vmin, nv)
        d = diagram_distance(S_vf0, S_vf, finv, v, offset[-1]-offset[0], distance_type)
        misfit=sqrt(mean((abs(d-d0))**order))
        Pmo[l]=exp(-misfit)
#        print(mscal, misfit)
        
    minlim1=amin(mo[0:n0,:],axis=0)
    maxlim2=amax(mo[0:n0,:],axis=0)
    
    nmo=n0

    pchrono[0:nmo]=Pmo[0:nmo]

    #Sorting models according to increasing probability
    temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
    temp = temp[temp[:,0].argsort(),]

    Pmo[0:nmo] = temp[0:nmo,0]
    mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]
    
    box1=zeros(nm)
    box2=zeros(nm)

    for i in range(ni) : # loop on number of iterations
        
        nbcell=nmo # number of cells
        #z2=zeros(nbcell)
        x=zeros(nbcell)
    
        #Boundary values for n1 best parameters 
        box1=amin(mo[nmo-n1:nmo,0:nm],axis=0)
        box2=amax(mo[nmo-n1:nmo,0:nm],axis=0)
        
        #Descaling
        mo=mo*Ascal+Bscal
        minlim1=minlim1*Ascal+Bscal
        maxlim2=maxlim2*Ascal+Bscal
        box1=box1*Ascal+Bscal
        box2=box2*Ascal+Bscal
#        print(box1, box2)
        #New scale
        Ascal=box2-box1
        Bscal=box1    
#        print(Ascal, Bscal)
        Ascal[Ascal==0]=1
    
        minlim1=(minlim1-Bscal)/Ascal
        maxlim2=(maxlim2-Bscal)/Ascal
   
        #Rescaling 
        mo=(mo-Bscal)/Ascal
        
        #In the n1 best cells
        for icell in range(nbcell-n1,nbcell) :  
            #print(icell, mo[icell,:]*Ascal+Bscal)
            z2=0
        
            for k in range(1,nm) :
                z2=z2+(mo[0:nbcell,k]-mo[icell,k])**2
                    
            #current model
            mocur=copy(mo[icell,:])

            #Sampling n2 models in each n1 best cell
            for j in range(n2) :
                
                for k in range(nm) :
                    
                    #computation of distances
                    x[icell]=100*maxlim2[k] #Large arbitrary number for the cell we are in
                    index=arange(nbcell)
                    index=index[index!=icell] #all other cells than icell
                    x[index]=1/2*(mo[icell,k]+mo[index,k]+(z2[icell]-z2[index])/(mo[icell,k]-mo[index,k]))

                    if x[x<=mocur[k]].shape[0]==0 : #cell on the edge
                        l1=minlim1[k]
                    else : #cell not on the edge
                        l1=amax(x[x<=mocur[k]])

                    if x[x>=mocur[k]].shape[0]==0 : #cell on the edge
                        l2=maxlim2[k]
                    else : #cell not on the edge
                        l2=amin(x[x>=mocur[k]])
                
                    #Probably not necessary :
                    if l1<minlim1[k] :
                        l1=minlim1[k]
                    if l2>maxlim2[k] :
                        l2=maxlim2[k]
                
                    #New sampled model : random walk from previous model
                    mo[nmo,:]=copy(mocur)
                    mo[nmo,k]=random.random()*(l2-l1)+l1
                
                    if k==nm-1 :
                        knew=0
                    else :
                        knew=k+1
                    #New computation of distances
                    z2=z2+(mo[0:nbcell,k]-mo[nmo,k])**2-(mo[0:nbcell,knew]-mo[nmo,knew])**2
                
                    mocur=copy(mo[nmo,:])

                mscal = mo[nmo,:]*Ascal+Bscal 
                vph = 1/(gpdc_calculation(finv, cvp, mscal[0:2], dns, append(mscal[2], 0.0))[:,1])
                s_tx = seismo_tx_filter(t, offset, signal, vph, fmin, fmax)
                _, _, S_vf = diagram_v_f(s_tx, offset, t, fmin, fmax, vmax, vmin, nv)
                d = diagram_distance(S_vf0, S_vf, finv, v, offset[-1]-offset[0], distance_type)
                misfit=sqrt(mean((abs(d-d0))**order))
                Pmo[nmo]=exp(-misfit)
                    
                pchrono[nmo]=Pmo[nmo]
                nmo=nmo+1
        #Sorting
        temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
        temp = temp[temp[:,0].argsort(),]

        Pmo[0:nmo] = temp[0:nmo,0]
        mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    for k in range(nm) :  
        mo[:,k]=mo[:,k]*Ascal[k]+Bscal[k] 

    return mo,Pmo,pchrono

#%%
def sensitivity_inversion(d0,sensitivity,mmin,mmax,ni,n0,n1,n2):

    #Input 
    # d0 : vector of observed data
    # err : vector of data error bars
    # mmin : vector of minimum allowed values for model parameters
    # mmax : vector of maximum allowed values for model parameters
    # ni : number of iterations
    # n0 : number of initial random model samples
    # n1 : number of best models used at each iteration
    # n2 : number of new models in the neighborhood of previous best models
    # realist_err : realistic error to select models
    
    #Output 
    #mo : array of all tested model parameters (sorted by ascending probability P = exp(-misfit))
    #Pmo : vector of model probability, sorted by ascending probability
    #pchrono : vector of model probability, chronologically stored 
    layer = int((len(mmin)+1)/2)
    order=2; #Order of the misffit (2 = Gaussian errors & Least-Square)

    [nm]=mmin.shape #number of parameters 
    nmo=n0+ni*n1*n2    #Total number of models

    mo=zeros((nmo,nm)) #tested models 
    Pmo=zeros(nmo) #model probability = exp(-misfit)
    pchrono=zeros(nmo) #Chronological evolution of the probability
    #Scaling factors
    Ascal =  mmax - mmin 
    Bscal = mmin

    #n0 intitial random models 
    mo[0:n0,0:nm]=random.rand(n0,nm)
    for l in range(n0): # loop on initial random models
        
        mscal = mo[l,:]*Ascal+Bscal 
        d = sensitivity[0,:,0]*mscal[0] + sensitivity[1,:,0]*mscal[1]
        misfit=sqrt(mean((abs(d-d0))**order))
        Pmo[l]=exp(-misfit)
#        compare = greater(realist_err, abs(d-d0))   # compare difference with giving error
#        m_err[l,:] = mscal*(sum(compare)==len(freq))
#        P_err[l] = exp(-misfit)*(sum(compare)==len(freq))
       
    # max and min values in random models    
    minlim1=amin(mo[0:n0,:],axis=0)
    maxlim2=amax(mo[0:n0,:],axis=0)

    nmo=n0

    pchrono[0:nmo]=Pmo[0:nmo]

    #Sorting models according to increasing probability
    temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
    temp = temp[temp[:,0].argsort(),]

    Pmo[0:nmo] = temp[0:nmo,0]
    mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    box1=zeros(nm)
    box2=zeros(nm)

    for i in range(ni) : # loop on number of iterations

        nbcell=nmo # number of cells
        #z2=zeros(nbcell)
        x=zeros(nbcell)
    
        #Boundary values for n1 best parameters 
        box1=amin(mo[nmo-n1:nmo,0:nm],axis=0) # nm: number of parameters
        box2=amax(mo[nmo-n1:nmo,0:nm],axis=0)

        #Descaling
        mo=mo*Ascal+Bscal
        minlim1=minlim1*Ascal+Bscal
        maxlim2=maxlim2*Ascal+Bscal
        box1=box1*Ascal+Bscal
        box2=box2*Ascal+Bscal
    
        #New scale
        Ascal=box2-box1
        Bscal=box1    
        Ascal[Ascal==0]=1
    
        minlim1=(minlim1-Bscal)/Ascal
        maxlim2=(maxlim2-Bscal)/Ascal
   
        #Rescaling 
        mo=(mo-Bscal)/Ascal

        #In the n1 best cells
        for icell in range(nbcell-n1,nbcell) :  

            z2=0
        
            for k in range(1,nm) : # loop on number of parameters
                z2=z2+(mo[0:nbcell,k]-mo[icell,k])**2
        
            #current model
            mocur=copy(mo[icell,:])

            #Sampling n2 models in each n1 best cell
            for j in range(n2) : # loop on number of new models
                
                for k in range(nm) : # loop on number of parameters
                    
                    #computation of distances
                    x[icell]=100*maxlim2[k] #Large arbitrary number for the cell we are in
                    index=arange(nbcell)
                    index=index[index!=icell] #all other cells than icell
                    x[index]=1/2*(mo[icell,k]+mo[index,k]+(z2[icell]-z2[index])/(mo[icell,k]-mo[index,k]))

                    if x[x<=mocur[k]].shape[0]==0 : #cell on the edge
                        l1=minlim1[k]
                    else : #cell not on the edge
                        l1=amax(x[x<=mocur[k]])

                    if x[x>=mocur[k]].shape[0]==0 : #cell on the edge
                        l2=maxlim2[k]
                    else : #cell not on the edge
                        l2=amin(x[x>=mocur[k]])
                
                    #Probably not necessary :
                    if l1<minlim1[k] :
                        l1=minlim1[k]
                    if l2>maxlim2[k] :
                        l2=maxlim2[k]
                
                    #New sampled model : random walk from previous model
                    mo[nmo,:]=copy(mocur)
                    mo[nmo,k]=random.random()*(l2-l1)+l1
                
                    if k==nm-1 :
                        knew=0
                    else :
                        knew=k+1
                    #New computation of distances
                    z2=z2+(mo[0:nbcell,k]-mo[nmo,k])**2-(mo[0:nbcell,knew]-mo[nmo,knew])**2
                
                    mocur=copy(mo[nmo,:])

                mscal=mo[nmo,:]*Ascal+Bscal
                d=sensitivity[0,:,0]*mscal[0] + sensitivity[1,:,0]*mscal[1]
                misfit=sqrt(mean((abs(d-d0))**order))
                Pmo[nmo]=exp(-misfit)
                    
                pchrono[nmo]=Pmo[nmo]
                nmo=nmo+1
        #Sorting
        temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
        temp = temp[temp[:,0].argsort(),]

        Pmo[0:nmo] = temp[0:nmo,0]
        mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    for k in range(nm) :  
        mo[:,k]=mo[:,k]*Ascal[k]+Bscal[k] 

    return mo,Pmo,pchrono#,m_err,P_err

#%%
def TLI_vph_inversion(d0,vph0,mmin,mmax,ni,n0,n1,n2,dns,cvp,freq):#,realist_err=0,vph_ref=0,opt = 1):

    #Input 
    # d0 : vector of observed data distance
    # err1 : error of baseline data
    # err2 : error of repeatline data
    # mmin : vector of minimum allowed values for model parameters
    # mmax : vector of maximum allowed values for model parameters
    # ni : number of iterations
    # n0 : number of initial random model samples
    # n1 : number of best models used at each iteration
    # n2 : number of new models in the neighborhood of previous best models
    # cvs0,h0 : inversed model of baseline
    # freq : frequency vector with a certain given range
    
    
    #Output 
    #mo : array of all tested model parameters (sorted by ascending probability P = exp(-misfit))
    #Pmo : vector of model probability, sorted by ascending probability
    #pchrono : vector of model probability, chronologically stored 
    
    order=2; #Order of the misffit (2 = Gaussian errors & Least-Square)
    
    [nm]=mmin.shape #number of parameters 
    nmo=n0+ni*n1*n2    #Total number of models

    mo=zeros((nmo,nm)) #tested models 
    Pmo=zeros(nmo) #model probability = exp(-misfit)
    pchrono=zeros(nmo) #Chronological evolution of the probability
    #Scaling factors
    Ascal =  mmax - mmin 
    Bscal = mmin

    #n0 intitial random models 
    mo[0:n0,0:nm]=random.rand(n0,nm)
    for l in range(n0):
        
        mscal = mo[l,:]*Ascal+Bscal 
        vph = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns, append(mscal[2], 0.0))[:,1])
        d = vph0-vph
        misfit=sqrt(mean((abs(d-d0))**order))
        Pmo[l]=exp(-misfit)
#        print(mscal, misfit)
        
    minlim1=amin(mo[0:n0,:],axis=0)
    maxlim2=amax(mo[0:n0,:],axis=0)
    
    nmo=n0

    pchrono[0:nmo]=Pmo[0:nmo]

    #Sorting models according to increasing probability
    temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
    temp = temp[temp[:,0].argsort(),]

    Pmo[0:nmo] = temp[0:nmo,0]
    mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]
    
    box1=zeros(nm)
    box2=zeros(nm)

    for i in range(ni) : # loop on number of iterations
        
        nbcell=nmo # number of cells
        #z2=zeros(nbcell)
        x=zeros(nbcell)
    
        #Boundary values for n1 best parameters 
        box1=amin(mo[nmo-n1:nmo,0:nm],axis=0)
        box2=amax(mo[nmo-n1:nmo,0:nm],axis=0)
        
        #Descaling
        mo=mo*Ascal+Bscal
        minlim1=minlim1*Ascal+Bscal
        maxlim2=maxlim2*Ascal+Bscal
        box1=box1*Ascal+Bscal
        box2=box2*Ascal+Bscal
#        print(box1, box2)
        #New scale
        Ascal=box2-box1
        Bscal=box1    
#        print(Ascal, Bscal)
        Ascal[Ascal==0]=1
    
        minlim1=(minlim1-Bscal)/Ascal
        maxlim2=(maxlim2-Bscal)/Ascal
   
        #Rescaling 
        mo=(mo-Bscal)/Ascal
        
        #In the n1 best cells
        for icell in range(nbcell-n1,nbcell) :  
            #print(icell, mo[icell,:]*Ascal+Bscal)
            z2=0
        
            for k in range(1,nm) :
                z2=z2+(mo[0:nbcell,k]-mo[icell,k])**2
                    
            #current model
            mocur=copy(mo[icell,:])

            #Sampling n2 models in each n1 best cell
            for j in range(n2) :
                
                for k in range(nm) :
                    
                    #computation of distances
                    x[icell]=100*maxlim2[k] #Large arbitrary number for the cell we are in
                    index=arange(nbcell)
                    index=index[index!=icell] #all other cells than icell
                    x[index]=1/2*(mo[icell,k]+mo[index,k]+(z2[icell]-z2[index])/(mo[icell,k]-mo[index,k]))

                    if x[x<=mocur[k]].shape[0]==0 : #cell on the edge
                        l1=minlim1[k]
                    else : #cell not on the edge
                        l1=amax(x[x<=mocur[k]])

                    if x[x>=mocur[k]].shape[0]==0 : #cell on the edge
                        l2=maxlim2[k]
                    else : #cell not on the edge
                        l2=amin(x[x>=mocur[k]])
                
                    #Probably not necessary :
                    if l1<minlim1[k] :
                        l1=minlim1[k]
                    if l2>maxlim2[k] :
                        l2=maxlim2[k]
                
                    #New sampled model : random walk from previous model
                    mo[nmo,:]=copy(mocur)
                    mo[nmo,k]=random.random()*(l2-l1)+l1
                
                    if k==nm-1 :
                        knew=0
                    else :
                        knew=k+1
                    #New computation of distances
                    z2=z2+(mo[0:nbcell,k]-mo[nmo,k])**2-(mo[0:nbcell,knew]-mo[nmo,knew])**2
                
                    mocur=copy(mo[nmo,:])

                mscal = mo[nmo,:]*Ascal+Bscal 
                vph = 1/(gpdc_calculation(freq, cvp, mscal[0:2], dns, append(mscal[2], 0.0))[:,1])
                d = vph0-vph
                misfit=sqrt(mean((abs(d-d0))**order))
                Pmo[nmo]=exp(-misfit)
                    
                pchrono[nmo]=Pmo[nmo]
                nmo=nmo+1
        #Sorting
        temp = concatenate((Pmo[0:nmo,None],mo[0:nmo,0:nm]),axis=1)
        temp = temp[temp[:,0].argsort(),]

        Pmo[0:nmo] = temp[0:nmo,0]
        mo[0:nmo,0:nm] = temp[0:nmo,1:nm+1]

    for k in range(nm) :  
        mo[:,k]=mo[:,k]*Ascal[k]+Bscal[k] 

    return mo,Pmo,pchrono


