#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 14:31:43 2018

@author: wangao

Modification log : 
28/08/2018 : seismo_t_x
             frequency vector in Discrte Fourier Transform should start from 0 and contain Nf = Nt point,
             which makes last frequency = fe-df (or fe/2 -df, depends on situation)
18/09/2019 : 1. seismo_t_x
             program modified by Damien Pageot, calculation of seismogram in frequency domain to accelerate 
             creat frequency vector using np.fft.rfftfreq to avoid errors while doing it manually
             2. gpdc_calculation
             thickness input vector could have same length as cvp or cvs 
27/09/2019 : histogram_distance
             calculate 1st lobe range theoretically
             resolve BHA and KLD bugs 
30/09/2019 : seismo_t_x
             add fmin and fmax when generating seismogram, for time saving propose
07/11/2019 : read_su_data
             read SU data and do the signal processing to output dispersion curve
13/04/2020 : diagram_distance
             1. using the theoretical limits of 1st lobe as calculation zone (copy of histogram_distance function)
             2. the distance of diagram in the inversion should have the same range of velocity, for the measured diagrams
                or the synthetic ones. 
             3. as histograms, the number of data should be identical. Normalization is not sufficient, normalized surface 
                is condidered. 
21/04/2020 : diagram_distance        
"""
from scipy.interpolate import interp1d
import numpy as np
import os
import scipy.signal
from dictances import kullback_leibler, bhattacharyya##, euclidean
from scipy.spatial.distance import cityblock, correlation, euclidean#, mahalanobis
import matplotlib.pyplot as plt
#%%
def ricker(f, shift_t, t):
    """
    Ricker function in time domain
    input:
        f: central frequency
        shift_t: signal shift in time domain
        t: time vector
    output:
        rickerwav: ricker wavelet in time domain
    """
    # >> Parameters
    sigma2 = (np.pi*f)**2
    delta2 = (t-shift_t)**2

    # >> Calculation
    rickerwav = (1.0-2.0*sigma2*delta2)*np.exp(-sigma2*delta2)

    return rickerwav
#%%
def seismo_t_x(t, x, signal, velocity, noise_ratio=0):
    """
    Calculation of synthetic seismogram in a layered medium by a given source
    
    input: 
        t : time vector
        x : offset vector
        signal : source signal in time domain
        velocity : vector of frequency who gives dispersion law of medium. 
                   velocity = cte if medium is homogeneous
        noise_ratio: SNR (signal-to-noise ratio, defined as (std(noise)/std(signal))**2)
    output:
        s_tx : seismogram, size = nt*nx
               
    """    
    
    # >> Get parameters nt, dt and nx from input
    nt = len(t)         # length of time vector
    dt = t[1] - t[0]    # time resomution
    nx = len(x)         # length of offset vector
    
    # >> Fast Fourier transform
    fftsignal = np.fft.rfft(signal)     # Real fast Fourier transform
    fftspace = np.fft.rfftfreq(nt, dt)  # Fourier transform sample frequencies
    nfft = len(fftspace)
    
    omega = 2.*np.pi*fftspace
    
    # >> Calculation
    gseismo = np.zeros((nfft, nx), dtype=np.complex)
    for ix in range(0, nx):
        gseismo[1:, ix] = 1/np.sqrt(x[ix])*fftsignal[1:]*np.exp(complex(0., -1.)*omega[1:]*x[ix]/velocity[:])

    
    if noise_ratio: # if noise is needed
        ref_std = np.std(abs(gseismo[:,0]))              # standard deviation of seismogram at first receiver
        noise_std = np.sqrt(noise_ratio*ref_std**2)      # standard deviation of noise
        for ix in range(nx): # loop on x
            noise_amp = np.random.normal(0,noise_std,nfft-1) # ramdom noise
            gseismo[1:, ix] = gseismo[1:, ix]+noise_amp

#            Noise = np.zeros(nt, dtype = float) # initialize the noise vector at each offset position
#            for jj in range(int(nf/2)): # loop on frequency
#                Noise = Noise + dt*noise_amp[jj]*np.cos(omega[jj]*t)
#            s_tx[:,i] = s_tx[:,i] + Noise # add noise in seismogram
        
    # >> Inverse real Fourier transform
    seismo = np.fft.irfft(gseismo, n=nt, axis=0)
    
    return(seismo)
#%%
def seismo_tx_filter(t, x, signal, velocity, fmin, fmax, noise_ratio=0):
    """
    Calculation of synthetic seismogram in a layered medium by a given source
    
    input: 
        t : time vector
        x : offset vector
        signal : source signal in time domain
        velocity : vector of frequency who gives dispersion law of medium. 
                   velocity = cte if medium is homogeneous
        noise_ratio: SNR (signal-to-noise ratio, defined as std(signal)/std(noise)**2)
    output:
        s_tx : seismogram, size = nt*nx
               
    """    
    
    # >> Get parameters nt, dt and nx from input
    nt = len(t)         # length of time vector
    dt = t[1] - t[0]    # time resomution
    nx = len(x)         # length of offset vector
    
    # >> Fast Fourier transform
    fftsignal = np.fft.rfft(signal)     # Real fast Fourier transform
    fftspace = np.fft.rfftfreq(nt, dt)  # Fourier transform sample frequencies
    nf2 = len(fftspace)
    
    findex = (fftspace>=fmin)&(fftspace<=fmax)
    nf = sum(findex)
    
    omega = 2.*np.pi*fftspace
    
    # >> Calculation
    gseismo = np.zeros((nf2, nx), dtype=np.complex)
    for ix in range(0, nx):
        gseismo[findex, ix] = fftsignal[findex]*np.exp(complex(0., -1.)*omega[findex]*x[ix]/velocity[:])

    
    if noise_ratio: # if noise is needed
        ref_std = np.std(abs(gseismo[:,0]))              # standard deviation of seismogram at first receiver
        noise_std = np.sqrt(noise_ratio*ref_std**2)      # standard deviation of noise
        for ix in range(nx): # loop on x
            noise_amp = np.random.normal(0,noise_std,nf) # ramdom noise
            gseismo[1:, ix] = gseismo[1:, ix]+noise_amp

    # >> Inverse real Fourier transform
    seismo = np.fft.irfft(gseismo, n=nt, axis=0)
    
    return(seismo)    
#%%
def diagram_v_f(s_tx, x, t, fmin, fmax, vmax, vmin, nv):
    """
    Dispersion diagram calculation by MASW measurement and Park(1998) method
    
    input: 
        s_tx: seismogram matrix in time-offset domain (dimension: nt*nx)
        x: receiver vector
        t: time vector
        fmin, fmax: frequency range in which dipersion is calculated
        vmax: maximum Rayleigh velocity 
        vmin: minimum Rayleigh velocity
        nv: number of velocity in [vmin, vmax] range
    
    output: 
        f_output: output frequency vector in [fmin, fmax] range
        diagram_pw: Rayleigh wave dispersion diagram (nv*nf)
        dispersion_pw: Rayleigh wave dispersion curve as function of frequency 
                       by picking maxinum values in Rayleigh diagram
    """
    # Get parameters from input
    nt = len(t)
    dt = t[1] - t[0]
    
    # Fast Fourier transform
    S_fx = np.fft.rfft(s_tx, axis=0)
    fftspace = np.fft.rfftfreq(nt, dt)
    dfft = fftspace[1]-fftspace[0]
    
    # Dispersion diagram preparation
    v = np.linspace(vmin, vmax, nv) # Velocity vector
#    nw = int((fmax-fmin)/dfft)+1    # Number of frequency components
    findex = (fftspace>=fmin)&(fftspace<=fmax)
    f_output = fftspace[findex] # frequency (output) vector
    nw = sum(findex)
    
    iwmin = int(fmin/dfft)+1         # Minimum frequency indice
    diagram_pw = np.zeros((nv, nw), dtype=np.float)
    
    ### Loop over velocities
    for iv in range(0, nv):
        tmp = np.zeros(nw, dtype=np.complex)
        ### Loop over frequencies
        for iw in range(0, nw):
            phase = complex(0., 1.)*2.*np.pi*x[:]*fftspace[iwmin+iw]/v[iv] # Phase
            tmp[iw] = np.sum(S_fx[iw+iwmin, :]/np.abs(S_fx[iw+iwmin, :])*np.exp(phase))
        diagram_pw[iv, :] = np.abs(tmp[:])
        
    index = np.argmax(diagram_pw, axis = 0)
    dispersion_pw = v[index]        
        
    return(f_output, dispersion_pw, diagram_pw)
#%%
def resample(freq, v, thk): 
   
    """
    resample(disp_curve, h_min, h_max, dhdisp_curve, h_min, h_max, dh)
    
    function to resample dispersion curve from linear frequency sampling to linear wavelength sampling

    input : 
            v : initial dispersion curve as function of frequency
            thk : vector of N layer thicknesses
            
    output : 
            save corresponding velocity and frequency vectors for resampled wavelength vector
    """
    LAMBDA=5 # max wavelength multiplier
    mode = v.shape[1]
    new_f = np.zeros([v.shape[0], v.shape[1]])
    new_v = np.zeros([v.shape[0], v.shape[1]])
    h_max = LAMBDA*(sum(thk)+thk[-1]) # maximum investigation depth
    for ii in range(mode):
        
        index = np.where(v[:,ii])[0]
        velocity = v[index,ii]
        wavelength = velocity/freq[index]      # initial dispersion curves
        h_min = min(wavelength)
        
        # interpolation function for wavelength-velocity
        wavelength_v_function = interp1d(wavelength, velocity, "linear") 
        # resample wavelength vector
        new_wavelength = np.linspace(h_min, h_max, len(index)) 
        # interpolated velocity vector for linear wavelength sampling
        new_v[index,ii] = wavelength_v_function(new_wavelength) 
        new_f[index,ii] = new_v[index,ii] / new_wavelength
        
    
    return(new_f)
#%%
def realiste_error(disp_curve, f, X_total):
    """calculation of realiste error (O'Neill thesis)
    
    input: 
        disp_curve: Rayleigh wave dispersion curve as function of frequency
        f: frequency vector
        X_total: total length of receivers
    output: 
        realiste_error: realistic error as function of frequency
    
    """
    dp = 1.0/(f*X_total)                # slowness resolution
    p = 1/disp_curve                         # slowness
    dc = np.abs(1/(p-dp/2) - 1/(p+dp/2))     # phase velocity resolution
    realiste_error = np.power(10, -0.5)*dc   # realistic dispersion error

    return(realiste_error)
    
#%%

def gpdc_calculation(f, cvp, cvs, dns, thk, nmode=1, velocity_type = "phase"):
    """
    calculate dispersion curve foa a given model with gpdc
    
    input: 
        f: freuqency band (attention! fmin couldn't be 0)
        cvp: compressional velocity vector
        cvs: shear velocity vector
        dns: density vector
        thk: layer thickness, last element should be 0
        nmode: number of mode, input as interger
        velocity_type: "phase" for phase velocity and "group" for group velocity
    """
    # >> Frequency parameters
    f_min = f[0]
    f_max = f[-1]
    freq_nr = len(f)
    
    # >> Medium parameters
    layer_nr = len(dns)
    
    # >> GPDC input preparation
    model_file = open("gpdc.model", 'w')
    model_file.write(str(layer_nr)+ "\n")
    for ii in range(0,layer_nr):
        model_file.write(str(thk[ii])+' ')      # Write thickness
        model_file.write(str(cvp[ii])+' ')      # Write P-wave velocity
        model_file.write(str(cvs[ii])+' ')      # Write S-wave velocity
        model_file.write(str(dns[ii])+'\n')     # Write density
    model_file.close()    
        
    
#    medium_model = np.zeros([layer_nr, 4])
#    medium_model[:,1] = cvp[:]
#    medium_model[:,2] = cvs[:]
#    medium_model[:,3] = dns[:]
#    medium_model[:-1,0] = thk[:]
#    medium_model[-1,0] = 0
#    
#    model_file = open("gpdc.model", 'w')
#    model_file.write(str(layer_nr)+ "\n")
#    model_file.write(str(medium_model).replace("[","").replace("]",""))
#    model_file.close()
    
    # >> Define GPDC shell script
    if velocity_type == "phase":
        gpdc_code = "gpdc -n {} -min " +str(f_min).replace(".",",") +" -max {} -R {} -s frequency < gpdc.model > gpdc.txt"
    elif velocity_type == "group":
        gpdc_code = "gpdc -n {} -min " +str(f_min).replace(".",",") +" -max {} -R {} -s frequency -group < gpdc.model > gpdc.txt"
    
    # >> Execute GPDC shell script
    os.system(gpdc_code.format(freq_nr, f_max, nmode))
    # >> Load GPDC result
    gpdc_dispersion_curve = np.loadtxt("gpdc.txt",comments='#')
    # >> Remove GPDC product files                       
    os.remove("gpdc.model")
    os.remove("gpdc.txt")                                      
    return(gpdc_dispersion_curve)
#%%
def group_velocity(v_ph, wavelength):
    """calculation of group velocity from a given phase velocity
    """
    
    #dlambda = np.gradient(wavelength)
    dvph_dlambda = np.gradient(v_ph, wavelength)#dlambda)
    
    v_g = v_ph - wavelength*dvph_dlambda
    
    return(v_g)
    
def dvph_df(vph, freq): 
    """calculate dV_ph/df
    """
    wavelength = vph/freq
    vg =group_velocity(vph, wavelength)
    dvph_df = vph*(vg-vph)/(freq*vg)
    return(dvph_df)
    
#%%
from sklearn.linear_model import LinearRegression 

def MFA(s_tx, t, offset, fi, Gamma):
    """MFA (Multiple Filter Analysis) method to calculate dispersion curve of group velocity
    from a measured seismogram.
    
    s_tx : seismogram in time-space domain with size nt*n_rcp
    fi : frequency band in which group velocity is calculated
    Gamma : filter width control parameter, can be constant or variable as frequency changes
    t : time vector
    offset : space vector, positions of first recepter to last one
    """
    
    nt, n_rcp = s_tx.shape  # number in time and of recepters 
    nf = nt*4   # frequency number
    
    dt = t[1] - t[0]  # temporal resolution
    fs = 1/dt   #sampling frequency 
    f = np.linspace(0, fs, nf, endpoint = False)  # frequency vector
    
    vg = np.zeros(len(fi))
#    vg_diagram = np.zeros([len(fi),n_rcp])
    
    S_fx = np.fft.fft(s_tx, n=nf, axis = 0) #1D fourier transform
    
    for jj in range(len(fi)):
        sg = np.zeros([n_rcp, nt], dtype=complex)  # sg = convolve(s, g)
        for ii in range(n_rcp):
            
            gamma = Gamma[jj]
            
            # initialise gaussian filter in frequency domain
            G = np.zeros(int(nf/2), dtype = complex)
            # filtered signal in frequency domain
            SG = np.zeros(len(f), dtype = complex)
        
            # gaussian filter
            G = np.exp(-1/2*((f[0:int(nf/2)] - fi[jj])/(gamma*fi[jj]))**2) 
        
            # filtered signals in frequency domain : 0<f<fs/2
            SG[0:int(len(f)/2)] = S_fx[0:int(nf/2),ii]*G 
        
            # conjugate signals in frequency domain : fs/2<f<fs
            SG[int(len(f)/2)+1:] = np.conj(SG[int(nf/2)-1:0:-1])
        
            # filtered signals in time domain
            sgi = np.fft.ifft(SG, nf)[0:nt] 
            
            # water level to avoid numerical instability
            sgi[sgi==0] = 1e-20*(1+1j)
            
            sg[ii,:] = abs(sgi)**2
         
        # linear regression
        reg = LinearRegression().fit(t[np.argmax(sg, axis = 1)].reshape(-1, 1), offset)
        vg[jj] = reg.coef_[0]

    return(vg)

#%%
def histogram_distance(diagram_baseline, diagram_repeatline, f, v, rcvlength, distance_type = "KLD", fmode = 80, vmode = 800):
    """
    Calculate difference between two dispersion diagrams (baseline and repeatline) at each frequency by means of 
    histogram distance calculation. Only fondamental mode is taken into account.
    input: 
        diagram_baseline: dispersion diagram of baseline, size = nv*nf
        diagram_repeatline: dispersion diagram of repeatline
        f: frequency vector, len(f) = nf
        fmin, fmax: frequency range in which distances are calculating
        v: velocity vector, len(v) =nv
        rcvlength: length of receiver array, rcvlength = x[-1]-x[0]
        distance_type: 
            KLD: Kullback–Leibler Divergence (Distance)
            BHA: Bhattacharyya Distance
            L1: L1 norm distance
            L2: L2 norm distance
        fmode: frequency at which there is a mode jump
        vmode: velocity at which two modes are completely seperated
    """
    nv = len(v)
    # 1st lobe index
    nn = np.array([1, -1])
    
    # output distance
    D = np.zeros(len(f))
    
    # data normalization
    data_baseline = diagram_baseline/diagram_baseline.max(axis = 0)
    data_repeatline = diagram_repeatline/diagram_repeatline.max(axis = 0)
    
    # loop on frequency range
    for ii in range(len(f)): 
        # initialized diagram
        d_baseline = np.zeros(nv)
        d_repeatline = np.zeros(nv)
        
        # maximum velocity for fundamental mode
        if f[ii]<=fmode: 
            v1max = data_baseline[:,ii].argmax()
            v2max = data_repeatline[:,ii].argmax()
        else:
            v1max = data_baseline[v<=vmode,ii].argmax()
            v2max = data_repeatline[v<=vmode,ii].argmax()
        v1max = v[v1max]
        v2max = v[v2max]
        
        # calculate the 1st lobe range 
        v1range = 1/(1/v1max+nn/(f[ii]*rcvlength))
        index1 = np.where(v>v1range[0])[0][0]
        index2 = np.where(v<v1range[1])[0][-1]
        
        v2range = 1/(1/v2max+nn/(f[ii]*rcvlength))
        index3 = np.where(v>v2range[0])[0][0]
        index4 = np.where(v<v2range[1])[0][-1]
        
        vminindex = min(index1, index3)
        vmaxindex = max(index2, index4)
        # normalized diagrams
        d_baseline[index1:index2+1] = diagram_baseline[index1:index2+1,ii]/sum(diagram_baseline[index1:index2+1,ii])
        d_repeatline[index3:index4+1] = diagram_repeatline[index3:index4+1,ii]/sum(diagram_repeatline[index3:index4+1,ii])
        
        baseline = d_baseline[vminindex:vmaxindex+1]
        repeatline = d_repeatline[vminindex:vmaxindex+1] 
        
        # data transfer to dictionary for later calculation
        P = {index1+jj: d_baseline[index1+jj] for jj in range(index2+1-index1)}
        Q = {index3+jj: d_repeatline[index3+jj] for jj in range(index4+1-index3)}
        # Distance calculation
        Distance = {
                "KLD": kullback_leibler(P,Q),
                "BHA": bhattacharyya(P,Q),
#                "EUC": euclidean(P,Q),  # EUC in dictances is the same as in scipy.spatial.distance
                
                "EUC": euclidean(baseline, repeatline),
                "COR": correlation(baseline, repeatline),
                "block": cityblock(baseline, repeatline),
#                "MAH": mahalanobis(data_baseline[:,ii], data_repeatline[:,ii], np.linalg.pinv(cov_mat))
                "ordinary": np.sum(abs(np.cumsum(baseline-repeatline)))
                    }
        D[ii] = Distance.get(distance_type)
        
    return(D)
#%%

def diagram_distance(diagram_baseline, diagram_repeatline, f, v, rcvlength, distance_type = "KLD", fmode = 80, vmode = 800):
    """
    Calculate diagram difference between two dispersion diagrams (baseline and repeatline) at each frequency by means of 
    histogram distance calculation. Only fondamental mode/principal lobe of each data is taken into account, 
    the amplitude beyound the principal lobe ranges are set to zero.
    !!!! No absolute value for Ordinal type distance !!!!
    input: 
        diagram_baseline: dispersion diagram of baseline, size = nv*nf
        diagram_repeatline: dispersion diagram of repeatline
        f: frequency vector, len(f) = nf
        fmin, fmax: frequency range in which distances are calculating
        v: velocity vector, len(v) =nv
        rcvlength: length of receiver array, rcvlength = x[-1]-x[0]
        distance_type: 
            KLD: Kullback–Leibler Divergence (Distance)
            BHA: Bhattacharyya Distance
            L1: L1 norm distance
            L2: L2 norm distance
        fmode: frequency at which there is a mode jump
        vmode: velocity at which two modes are completely seperated
    """
    nv = len(v)
    # 1st lobe index
    nn = np.array([1, -1])
    
    # output distance
    D = np.zeros(len(f))
    
    # loop on frequency range
    for ii in range(len(f)): 
        # initialized diagram
        baseline = np.zeros(nv)
        repeatline = np.zeros(nv)
        
        # maximum velocity for fundamental mode
        if f[ii]<=fmode: 
            v1max = diagram_baseline[:,ii].argmax()
            v2max = diagram_repeatline[:,ii].argmax()
        else:
            v1max = diagram_baseline[v<=vmode,ii].argmax()
            v2max = diagram_repeatline[v<=vmode,ii].argmax()
        v1max = v[v1max]
        v2max = v[v2max]
        
        # calculate the 1st lobe range 
        v1range = 1/(1/v1max+nn/(f[ii]*rcvlength))
        index1 = np.where(v>v1range[0])[0][0]
        index2 = np.where(v<v1range[1])[0][-1]
        
        v2range = 1/(1/v2max+nn/(f[ii]*rcvlength))
        index3 = np.where(v>v2range[0])[0][0]
        index4 = np.where(v<v2range[1])[0][-1]
        
        # normalized surface of diagrams
        baseline[index1:index2+1] = diagram_baseline[index1:index2+1,ii]/sum(diagram_baseline[index1:index2+1,ii])
        repeatline[index3:index4+1] = diagram_repeatline[index3:index4+1,ii]/sum(diagram_repeatline[index3:index4+1,ii])       
       
        # data transfer to dictionary for later calculation
        P = {index1+jj: baseline[index1+jj] for jj in range(index2+1-index1)}
        Q = {index3+jj: repeatline[index3+jj] for jj in range(index4+1-index3)}
        # Distance calculation
        Distance = {
                "KLD": kullback_leibler(P,Q),
                "BHA": bhattacharyya(P,Q),
#                "EUC": euclidean(P,Q),  # EUC in dictances is the same as in scipy.spatial.distance
                
                "EUC": euclidean(baseline, repeatline),
                "COR": correlation(baseline, repeatline),
                "block": cityblock(baseline, repeatline),
#                "MAH": mahalanobis(data_baseline[:,ii], data_repeatline[:,ii], np.linalg.pinv(cov_mat))
                "ordinary": np.sum(np.cumsum(baseline-repeatline)) #np.sum(abs(np.cumsum(baseline-repeatline)))
                    }
        D[ii] = Distance.get(distance_type)
        
    return D      
    
#%%
    
from nessi.io import suread #suread2 as

def read_su_data(filename, dx, offset, tmax, tmin, fmax, fmin, vmax, vmin, nv, freqfilter, ampsfilter, detail="off"):
    """ function to read SU data by nessi package and output dispersion curve after signal processing
    input:
        filename: filename.su
        dx: space between receivers
        offset: first receiver position from the source
        tmax, tmin: time window function, to cut the unnecessary signal
        fmax, fmin: frequency range 
        vmax, vmin: velocity range when calculate dispersion curve
        nv: number of velocity -> velocity resolution
        freqfilter: frequency vector for filter, which contains 4 elements, see sufilter for more details
        ampsfilter: amplitude vector for filter, which contains 4 elements of 0 and 1, see sufilter for more details
        detail: switch for figure plots
    
    """
    #-> read data
    data = suread(filename)
    
    # copy data for signal processing
    dobs = data.copy()

    # delete average value
    dobs.operation(type='avg')
    # windows function to cut non useful data
    #dobs.wind(tmin=tmin, tmax=tmax)
    # filter
    dobs.pfilter(freq=freqfilter, amps=ampsfilter)
    
    #-> get traces information
    trace = dobs.traces

    # for the case where first trace is zero
    s_tx = trace[1:, :].T
    ns, ntraces = s_tx.shape
    
    #-> offset vector
    x = np.linspace(offset, (ntraces-1)*dx+offset, ntraces)
    
    # read header of first trace, gives sample time dt in [us]
    dt = dobs.header[0]['dt']/1e6 
    t = np.linspace(0, (ns-1)*dt, ns)

    f_out,vph,diagram = diagram_v_f(s_tx, x, t, fmin, fmax, vmax, vmin, nv) 

    if detail=="on":
        plt.figure()
        dobs.wiggle(skip=5, tracecolor='red',label1='Time [ms]', label2='Trace')
        data.wiggle(skip=5, tracecolor='black',label1='Time [ms]', label2='Trace')
        
        v = np.linspace(vmin, vmax, nv)
        ff, vv = np.meshgrid(f_out, v)
        plt.figure()
        plt.contourf(ff, vv, diagram)
        plt.plot(f_out, vph, "k.")
        plt.xlabel("Frequency [Hz]"); plt.ylabel("Velocity [m/s]")
        plt.show()
    return(f_out,s_tx,vph,diagram)
    #return(s_tx)













































