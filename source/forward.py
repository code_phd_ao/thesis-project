#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 15:26:14 2018

@author: Ao Wang

% Usage :
% 		vr = mat_disperse(thk,dns,cvp,cvs,freq)
% 		[vr,z,r] = mat_disperse(thk,dns,cvp,cvs,freq)
% 		[vr,z,r,dvrvs] = mat_disperse(thk,dns,cvp,cvs,freq)
% 		[vr,z,r,dvrvs,vre,dvrevs] = mat_disperse(thk,dns,cvp,cvs,freq,offsets)
% 		[vr,z,r,dvrvs,vre,dvrevs,ur,uy] = mat_disperse(thk,dns,cvp,cvs,freq,offsets)
%
% Required input parameters :
%	thk:	vector of N layer thicknesses
%	dns:	vector of N+1 layer mass densities
%	cvp:	vector of N+1 complex-valued layer compression wave velocities
%	cvs:	vector of N+1 complex-valued layer shear wave velocities
%	freq:	vector of M frequencies in Hz
%
% Required output parameters :
%	vr:		matrix of modal phase velocities (M by MAXROOT)
%
% Optional output parameters :
%	z:		matrix of depths (NUMPOINTS by M)
%	r:		matrix of displacement-stress vectors (M by MAXROOT by NUMPOINTS by 4)
%	dr:		matrix of displacement-stress derivatives (M by MAXROOT by NUMPOINTS by 2)
%	U:		matrix of group velocities (M by MAXROOTS)
%	zdvrvs:		matrix of partial derivatives at individual depths (M by MAXROOT by NUMPOINTS)
%	zdvrvp:		matrix of partial derivatives at individual depths (M by MAXROOT by NUMPOINTS)
%	dvrvs:		matrix of partial derivatives for each layer (M by MAXROOT by N+1)
%	dvrvp:		matrix of partial derivatives for each layer (M by MAXROOT by N+1)
%	vre:		matrix of effective vertical phase velocities (M by P)
%	dvrevs:		matrix of partial derivatives of vre for each layer (M by P by N+1)
%	dvrevp:		matrix of partial derivatives of vre for each layer (M by P by N+1)
%	ur:		matrix of horizontal Rayleigh wave displacements (M by P)
%	uy:		matrix of vertical Rayleigh wave displacements (M by P)
%
%
Modification log:
    11/10/2019: bug occurs when calculate dr in disp_stress. 
                when calculate np.gradient(vec1, a), a should be either constant scalair, or a vector which has no identical item
                temp, dr[:,:,jj,m] = numpy.gradient(numpy.squeeze(r[0:2,:,jj,m]),1,numpy.gradient(z[:,jj])) changes to
                temp, dr[:,:,jj,m] = numpy.gradient(numpy.squeeze(r[0:2,:,jj,m]),1,numpy.gradient(z[:,jj])[0]) since vector z has equal spaces
    11/10/2019: use gpdc to calculate dispersion curve (instead of function modal)
"""

#%%
import numpy
from scipy.optimize import fminbound
import cmath 
from scipy.interpolate import interp1d

import sys
sys.path.insert(0, '/home/wangao/Documents/thesis/') #'/home/aowang/Documents/thesis')# 
import math_func as mathf
#%% global parameters

# Tolerance for declaring a zero
global TOL
TOL = 1e-2
# Assumed maximum number of modes at any frequency
global MAXROOT
MAXROOT = 1
# Maximum number of increments to search between vrmin and vrmax
global NUMINC
NUMINC = 100
# Wavelength multiplier for calculating displacement-stress vectors (eigenfunctions)
global LAMBDA 
LAMBDA = 5
# Number of points (depths) for calculating displacement-stress vectors (eigenfunctions)
global NUMPOINTS
NUMPOINTS = 5000
#%%
def mat_disperse(thk, dns, cvp, cvs, freq, opt=1):
    
#    thk = numpy.reshape(thk,[len(thk),1])
#    dns = numpy.reshape(dns,[len(dns),1])
#    cvp = numpy.reshape(cvp,[len(cvp),1])
#    cvs = numpy.reshape(cvs,[len(cvs),1])
#    freq = numpy.reshape(freq,[len(freq),1])
    
    cvpmin = min(cvp)
    #   cvpmax = max(cvp)
    cvsmin = min(cvs)
    cvsmax = max(cvs)
    
    # Define the magnitudes and orientation of the source in the x, y, and z
    # directions and the depth of the source and receiver. Currently, only the algorithm for 
    # a vertical source at the surface is implemented. See Hisada (1994) for other options.
    Fx = 0
    Fy = 0
    Fz = 1.0
    Phi = 0
    s_depth = 0.0
    r_depth = 0.0
    
    #   Determine the minimum and maximum Rayleigh phase velocities in a homogeneous half space
    #   Note: the following empirical rules need further study (Lai 2003 Matlab)
    vrmin = 0.98*homogeneous(cvpmin,cvsmin)
    vrmax = cvsmax
    
#    vr = modal(freq,thk,dns,cvp,cvs,vrmin,vrmax)
    vr = 1/(mathf.gpdc_calculation(freq, cvp, cvs, dns, numpy.append(thk, 0.0))[:,1])
    vr = numpy.reshape(vr, [len(freq), 1])
    
    if opt==2: 
        [z,r,dr] = disp_stress(freq,vr,thk,dns,cvs,cvp,Fz)
        return(vr, z, r, dr)
    elif opt==3:
        [z,r,dr] = disp_stress(freq,vr,thk,dns,cvs,cvp,Fz)
        [U,zdvrvs,zdvrvp,zdvrrho,dvrvs,dvrvp,dvrrho] = partial(freq,vr,z,r,dr,thk,dns,cvs,cvp)
        return(vr,r,z,zdvrvs,zdvrvp,zdvrrho,dvrvs,dvrvp,dvrrho)
    else:
        return(vr)
    
#%%

def homogeneous(cvp,cvs) :
    """Calculation of Rayleigh wave velocity for a homogeneous medium
    Required input : 
        cvp : P-wave velocity of medium 
        cvs : S-wave velocity of medium 
    Output : 
        cvr : Rayleigh wave velocity
    """
    nu = 0.5*((cvp*cvp-2.0*cvs*cvs)/(cvp*cvp-cvs*cvs))    # Poisson's Ratio  

    # Definition of Coefficients of Rayleigh's Equation (e.g eq(3.3) Thesis Foti)
    a =  1
    b = -8
    c =  8*(3-2.0*(cvs*cvs)/(cvp*cvp))
    d = 16*((cvs*cvs)/(cvp*cvp)-1)

    # Solution of Rayleigh Equation
    p = [a,b,c,d]
    K2 = numpy.roots(p)
    cr = numpy.array([cvs*cmath.sqrt(ii) for ii in K2])

    # Determine which of the roots is correct using the estimated velocity (Achenbach, 1973)
    K = cvs*((0.862+1.14*nu)/(1+nu))
    index = numpy.argmin(abs(cr-K))
    cvr = cr[index]
    
    if not cvr :
        print('No root found for homogeneous half space')

    return(cvr)

#%%    
def modal(freq,thk,dns,cvp,cvs,crmin,crmax) : 
    """
    This function calculates the modal phase velocities in an elastic, 
    vertically heterogeneous medium using search techniques.
    """
    global MAXROOT, NUMINC, TOL
    
    vr = numpy.zeros([len(freq),MAXROOT], dtype = complex)
    
    for ii in range(len(freq)) :  # Loop through the frequencies
                
        if len(cvs) == 1 :
            vr[ii,0] = homogeneous(cvp,cvs)
        else :     
            numroot = 0
            om = 2*numpy.pi*freq[ii]
            
            # Establish the search parameters
            kmax = om/crmin
            kmin = om/crmax
            dk = (kmax - kmin)/NUMINC
        
            # Establish the first and second points
            k1 = kmax
            f1 = secular(k1,om,thk,dns,cvp,cvs)
            k2 = kmax - dk
            f2 = secular(k2,om,thk,dns,cvp,cvs)
        
            # Establish an arbitrary high value for kold
            kold = 1.1*kmax
            
            # Loop through the remaining points
            for m in range(NUMINC)[2:] :
                # print(m)
                k3 = kmax - m*dk
                f3 = secular(k3,om,thk,dns,cvp,cvs)
                
                #Determine if a minimum is bracketed
                if f2 < f1 and f2 < f3 :
                    
                    # Use golden search/parabolic interpolation to refine minimun
                    [ktrial,ftrial] = fminbound(secular, k3, k1, args = (om, thk, dns, cvp, cvs,), 
                                                                xtol = 1e-12, disp = 0, full_output = True)[0:2]
                    
                    # Check to see if ktrial is a zero and different from the previous zero
                   
                    if ftrial < TOL and abs((ktrial-kold)/kold) > 1e-2 :
                        
                        vr[ii,numroot] = om/ktrial
                        numroot = numroot + 1
                        kold = ktrial
                      
                # Break out of loop of maxroots is reached  
                if numroot == MAXROOT :
                    break
                
                k1 = k2 
                f1 = f2
                k2 = k3
                f2 = f3
    
    return(vr)
#%%    
def secular(k,om,thk,dns,cvp,cvs) :
    """
    Function defined in eq(31) by X.Chen(1993) 
    """
    # Check to see if the trial phase velocity is equal to the shear wave velocity
    # or compression wave velocity of one of the layers
    epsilon = 0.0001
    while any(abs(om/k-cvs)<epsilon) | any(abs(om/k-cvp)<epsilon) :
        k = k * (1+epsilon)
  
    [e11,e12,e21,e22,du,mu,nus,nup] = psv(thk,dns,cvp,cvs,om,k)
    [td,tu,rd,ru] = modrt(e11,e12,e21,e22,du)
    [Td,Rd] = genrt(td,tu,rd,ru)

    # Note that the absolute value of the secular function is calculated
    d = abs(numpy.linalg.det(e21[0,:,:] + numpy.matmul(numpy.matmul(e22[0,:,:],du[0,:,:]),Rd[0,:,:]))/(nus[0]*nup[0]*mu[0]**2))
    # Ru_0 = -inv(e21(:,:,1))*e22(:,:,1)*du(:,:,1)
    # d2 = abs( det(eye(2)-Ru_0*Rd(:,:,1)) )
    # d = abs(det(e21(:,:,1) + e22(:,:,1)*du(:,:,1)*Rd(:,:,1))/det(e21(:,:,1)))
    return(d) 
    
#%%    
def psv(thk,dns,cvp,cvs,om,k) :
    """
    This function calculates the E and Lambda matrices (up-going and down-going matrices) for the P-SV case.
    Note that a separate function, updown, is provided for calculating the Lambda matrices for use in 
    determining the displacement-stress vectors.
    """

    cvs2 = numpy.power(cvs, 2)
    cvp2 = numpy.power(cvp, 2)
    mu = dns*cvs2

    e11 = numpy.zeros([len(cvs), 2,2], dtype = complex)
    e12 = numpy.zeros([len(cvs), 2,2], dtype = complex)
    e21 = numpy.zeros([len(cvs), 2,2], dtype = complex)
    e22 = numpy.zeros([len(cvs), 2,2], dtype = complex)
    du = numpy.zeros([len(thk), 2,2], dtype = complex)

    if om == 0 :

       kappa = (1.0 + cvs2/cvp2)/(1.0 - cvs2/cvp2)
       kmu = k*mu
       
       e11[:,0,0] = numpy.ones([1,len(cvs)])
       e11[:,0,1] = e11[:,0,0]
       e12[:,0,0] = e11[:,0,0]
       e12[:,0,1] = e11[:,0,0]
       e11[:,1,0] = -(kappa - 1.0)
       e11[:,1,1] = e11[:,0,0]
       e12[:,1,0] = -e11[:,1,0]
       e12[:,1,1] = -e11[:,0,0]
       e21[:,0,0] = (kappa - 3.0)*kmu
       e21[:,0,1] = -2*kmu
       e22[:,0,0] = -e21[:,0,0]
       e22[:,0,1] = -e21[:,0,1]
       e21[:,1,0] = (kappa - 1.0)*kmu
       e21[:,1,1] = -2*kmu
       e22[:,1,0] = e21[:,1,0]
       e22[:,1,1] = e21[:,1,1]
       
       du[:,0,0] = numpy.exp(-k*thk)
       du[:,1,1] = numpy.exp(-k*thk)
       du[:,1,0] = -k*thk*numpy.exp(-k*thk)

    else : 
        k2 = k**2
        om2 = om**2
        ks2 = om2/cvs2
        nus = (k2-ks2+0j)**0.5# numpy.array([cmath.sqrt(k2-ii) for ii in ks2], dtype = complex)
        index = numpy.where(numpy.imag(-1j*nus) > 0)[0]
        nus[index] = -nus[index]
        gammas = nus/k
        kp2 = om2/cvp2
        nup = (k2-kp2+0j)**0.5# numpy.array([cmath.sqrt(k2-ii) for ii in kp2], dtype = complex)
        index = numpy.where(numpy.imag(-1j*nup) > 0)[0]
        nup[index] = -nup[index]
        gammap= nup/k
        chi = 2.0*k - ks2/k
        
        e11[:,0,0] = -numpy.ones([1,len(cvs)])
        e11[:,0,1] = gammas
        e12[:,0,0] = e11[:,0,0]
        e12[:,0,1] = gammas
        e11[:,1,0] = -gammap
        e11[:,1,1] = -e11[:,0,0]
        e12[:,1,0] = gammap
        e12[:,1,1] = e11[:,0,0]
        e21[:,0,0] = 2*mu*nup
        e21[:,0,1] = -mu*chi
        e22[:,0,0] = -e21[:,0,0]
        e22[:,0,1] = -e21[:,0,1]
        e21[:,1,0] = -e21[:,0,1]
        e21[:,1,1] = -2*mu*nus
        e22[:,1,0] = -e21[:,0,1]
        e22[:,1,1] = e21[:,1,1]

        du[:,0,0] = numpy.exp(-nup[0:len(thk)]*thk)
        du[:,1,1] = numpy.exp(-nus[0:len(thk)]*thk)
        
        
        
    return(e11,e12,e21,e22,du,mu,nus,nup) 
        
#%% 

def modrt(e11,e12,e21,e22,du):
    """
    This function calculates the modified R/T coefficients
    """
    
    # Determine the number of layers, N, not including the half space
    [N,m,n] = du.shape

    #Initialize a 4x4xN matrix
    X = numpy.zeros([N,4,4], dtype = complex)

    A = numpy.zeros([4,4], dtype = complex)
    B = numpy.zeros([4,4], dtype = complex)
    L = numpy.zeros([4,4], dtype = complex)
    M = numpy.zeros([4,2], dtype = complex)
    # Loop through the first N-1 layers (eq 24a)
    if N>1 : 
        
        for ii in range(N-1):
             
            A = numpy.append(numpy.append(e11[ii+1,:,:], -e12[ii,:,:], axis = 1),
                             numpy.append(e21[ii+1,:,:], -e22[ii,:,:], axis = 1) , axis = 0)
            B = numpy.append(numpy.append(e11[ii,:,:], -e12[ii+1,:,:], axis = 1),
                             numpy.append(e21[ii,:,:], -e22[ii+1,:,:], axis = 1) , axis = 0)
            L = numpy.append(numpy.append(du[ii,:,:], numpy.zeros([2,2]), axis = 1),
                             numpy.append(numpy.zeros([2,2]), du[ii+1,:,:], axis = 1) , axis = 0)
            M = numpy.matmul(B, L)
            X[ii,:,:] = numpy.linalg.solve(A, M)
        
        


    # Calculate the Nth layer (eq 24b)
    A = numpy.append(numpy.append(e11[-1,:,:], -e12[-2,:,:], axis = 1),
                         numpy.append(e21[-1,:,:], -e22[-2,:,:], axis = 1) , axis = 0)
    M = numpy.append(numpy.matmul(e11[-2,:,:],du[-1,:,:]), numpy.matmul(e21[-2,:,:],du[-1,:,:]) , axis = 0)
    X[-1,:,0:2] = numpy.linalg.solve(A, M)
    
    
    # Extract R/T submatrices

    td = X[:,0:2, 0:2] #T_d
    ru = X[:,0:2, 2:4] #R_ud
    rd = X[:,2:4, 0:2] #R_du
    tu = X[:,2:4, 2:4] # T_u
    return(td,tu,rd,ru)

#%%

def genrt(td,tu,rd,ru):
    """
    This function calculates the generalized R/T coefficients
    """
    
    [N,m,n] = td.shape

    # Initialize 2x2xN matrices
    Td = numpy.zeros([N,2,2], dtype = complex)
    Rd = numpy.zeros([N,2,2], dtype = complex)

    # Calculate the Td and Rd matrices for the Nth layer
    Td[-1,:,:] = td[-1,:,:]
    Rd[-1,:,:] = rd[-1,:,:]

    # Loop through the first N-1 layers in reverse order
    # eq (28), Chen
    for jj in reversed(range(N-1)): 
        Td[jj,:,:] = numpy.linalg.solve((numpy.eye(2) - numpy.matmul(ru[jj,:,:],Rd[jj+1,:,:])), td[jj,:,:])
        Rd[jj,:,:] = rd[jj,:,:] + numpy.matmul(numpy.matmul(tu[jj,:,:],Rd[jj+1,:,:]),Td[jj,:,:])

    
    return(Td,Rd)
#%%
    
def disp_stress(freq,vr,thk,dns,cvs,cvp,Fz):
    """
    This function calulates the displacement-stress vectors (i.e., the eigenfunctions)
    corresponding to the phase velocities (i.e., wavenumbers) contained in the vr matrix.
    (line 73, Yiran Ma modified the formula as (36) Chen, 1993, the origional
    code is different by some constant)
    """
    
    global NUMPOINTS, LAMBDA, MAXROOT
    
    vr_half = homogeneous(cvp[0],cvs[0])
    
    # Calculate the vector of circular frequencies
    om = 2*numpy.pi*freq

    # Determine the number of layers not including the half space
    N = len(thk)

    # Calulate the maximum depth for determining the displacement-stress vectors
    lambda_max = LAMBDA*(sum(thk)+thk[-1])*numpy.ones(len(freq))
    # method of Lai, depth too important as for low freq
#    lambda_max = LAMBDA*(numpy.real(vr[:,0])**2 + numpy.imag(vr[:,0])**2)/numpy.real(vr[:,0])/freq    
#    thk_max = (sum(thk)+thk[-1])*numpy.ones(len(lambda_max))
#    lambda_max[thk_max>lambda_max] = thk_max[thk_max>lambda_max]
#    lambda_max = max(numpy.append(sum(thk)+thk[-1],LAMBDA*(numpy.real(vr[:,0])**2 + numpy.imag(vr[:,0])**2)/numpy.real(vr[:,0])/freq))
#    lambda_max = numpy.array([round(ii) for ii in lambda_max])
    
    #Initiate the depth and displacement-stress vectors and their numerical derivatives
    z = numpy.zeros([NUMPOINTS,len(freq)])
    r = numpy.zeros([4,NUMPOINTS,len(freq),MAXROOT], dtype = complex)
    dr = numpy.zeros([2,NUMPOINTS,len(freq),MAXROOT], dtype = complex)
    
    # Loop through the frequencies
    for jj in range(len(freq)):
        
        # Create a vector of depths
        z[:,jj] = numpy.transpose(numpy.linspace(1e-6,lambda_max[jj],NUMPOINTS))
        step = numpy.floor(0.1/(z[1,jj]-z[0,jj])) 
        if step !=0:
            n_try = numpy.append(numpy.arange(0,NUMPOINTS-1,step, dtype = int), NUMPOINTS-1)
        else:
            n_try = numpy.array([NUMPOINTS-1], dtype = int)
        
        # Loop through the modes at each frequency
        index1 = numpy.where(vr[jj,:])[0]
        
        for m in range(len(index1)):
            
            # Calculate the wavenumber and load vector
            k = om[jj]/vr[jj,index1[m]]
            #delqz = numpy.array([0], [k*Fz/(2*numpy.pi)])
            
            # Check to see if the phase velocity is equal to the shear wave velocity 
            # or compression wave velocity of one of the layers
            epsilon = 0.0001;
            while any(abs(om[jj]/k-cvs)<epsilon) | any(abs(om[jj]/k-cvp)<epsilon): 
                k = k * (1+epsilon)
                
            # Calculate the PSV element matrices for each layer and generalized R/T matrices
            [e11,e12,e21,e22,du,mu,nus,nup] = psv(thk,dns,cvp,cvs,om[jj],k)
            [td,tu,rd,ru] = modrt(e11,e12,e21,e22,du)
            [Td,Rd] = genrt(td,tu,rd,ru)
            
            # Initialize the Cd and Cu matrices
            cd = numpy.zeros([N+1,2,1], dtype = complex)
            cu = numpy.zeros([N+1,2,1], dtype = complex)
            
            # Calculate Cd for the first layer
            [lamd,lamu] = updown(thk,cvp,cvs,om[jj],k,0,0)
            
            # fundamental mode
            if index1[m] == 0:
                if abs(vr[jj,0] - vr_half) > 1e-4:
                    F = numpy.linalg.solve(-e21[0,:,:],numpy.matmul(numpy.matmul(e22[0,:,:],du[0,:,:]),Rd[0,:,:]))
                    cd[0,:,:] = numpy.array([[F[0,1]],[1-F[0,0]]])/cmath.sqrt(abs(1-F[0,0])**2 + abs(F[0,1])**2 )
                else:
                    A = e21[0,:,:]/om[jj]
                    B = numpy.matmul(numpy.matmul(e22[0,:,:],du[0,:,:]),Rd[0,:,:])/om[jj]
                    cd[0,:,:] = numpy.array([[A[0,1]+B[0,1]], [-A[0,0]-B[0,0]]])/cmath.sqrt(abs(A[0,0]+B[0,0])**2 + abs(A[0,1]+B[0,1])**2)
            else:
                
                F = numpy.linalg.solve(-e21[0,:,:],numpy.matmul(numpy.matmul(e22[0,:,:],du[0,:,:]),Rd[0,:,:]))    
                cd[0,:,:] = numpy.array([[F[0,1]], [1-F[0,0]]])/cmath.sqrt( abs(1-F[0,0])**2 + abs(F[0,1])**2 )
                
                
            cu[0,:,:] = numpy.matmul(Rd[0,:,:],cd[0,:,:])             
            
            #Calculate Cd and Cu for the remaining layers
            
            for n in range(N-1):
                cd[n+1,:,:] = numpy.matmul(Td[n,:,:],cd[n,:,:])
                cu[n+1,:,:] = numpy.matmul(Rd[n+1,:,:],cd[n+1,:,:])
                
            cd[N,:,:] = numpy.matmul(Td[N-1,:,:],cd[N-1,:,:])
            
            # loop through the vector of depths
            
            for n in n_try:
                
                # Determine the layer corresponding to the current depth
                index2 = numpy.where(z[n,jj] <= numpy.append(numpy.cumsum(thk), z[NUMPOINTS-1,jj]))[0]
                layer = index2[0]
                
                
                # Calculate the up-going and down-going matrices for this depth
                [lamd,lamu] = updown(thk,cvp,cvs,om[jj],k,z[n,jj],layer)
                
                # Calculate the displacement-stress vector
                temp1 = numpy.append(numpy.append(e11[layer,:,:], e12[layer,:,:], axis = 1), 
                                     numpy.append(e21[layer,:,:], e22[layer,:,:], axis = 1), axis = 0)
                temp2 = numpy.append(numpy.append(lamd, numpy.zeros([2,2]), axis = 1), 
                                     numpy.append(numpy.zeros([2,2]), lamu, axis = 1), axis = 0)
                temp3 = numpy.append(cd[layer,:,:], cu[layer,:,:], axis = 0)
                r[:,n,jj,m] = numpy.matmul(numpy.matmul(temp1, temp2), temp3)[:,0]         
            
            if len(n_try) !=1:
                
                r1_try = abs(numpy.squeeze(r[0,list(n_try),jj,m]))
                Id = max((numpy.where(abs(numpy.diff(r1_try)) > 1e-6))[0])
            
                for n in range(int(n_try[Id])):
                
                    # Determine the layer corresponding to the current depth
                    index2 = numpy.where(z[n,jj] <= numpy.append(numpy.cumsum(thk), z[NUMPOINTS-1,jj]))[0]
                    layer = index2[0]
                
                
                    # Calculate the up-going and down-going matrices for this depth
                    [lamd,lamu] = updown(thk,cvp,cvs,om[jj],k,z[n,jj],layer)
                
                    # Calculate the displacement-stress vector
                    temp1 = numpy.append(numpy.append(e11[layer,:,:], e12[layer,:,:], axis = 1), 
                                         numpy.append(e21[layer,:,:], e22[layer,:,:], axis = 1), axis = 0)
                    temp2 = numpy.append(numpy.append(lamd, numpy.zeros([2,2]), axis = 1), 
                                         numpy.append(numpy.zeros([2,2]), lamu, axis = 1), axis = 0)
                    temp3 = numpy.append(cd[layer,:,:], cu[layer,:,:], axis = 0)
                    r[:,n,jj,m] = numpy.matmul(numpy.matmul(temp1, temp2), temp3)[:,0]  
                
                
                if n_try[Id]!=NUMPOINTS-1:
                    r[0,n_try[Id]:NUMPOINTS,jj,m] = interp1d(n_try[Id:],numpy.squeeze(r[0,list(n_try[Id:]),jj,m]))(numpy.arange(n_try[Id],NUMPOINTS,1))
                    r[1,n_try[Id]:NUMPOINTS,jj,m] = interp1d(n_try[Id:],numpy.squeeze(r[1,list(n_try[Id:]),jj,m]))(numpy.arange(n_try[Id],NUMPOINTS,1))
                    r[2,n_try[Id]:NUMPOINTS,jj,m] = interp1d(n_try[Id:],numpy.squeeze(r[2,list(n_try[Id:]),jj,m]))(numpy.arange(n_try[Id],NUMPOINTS,1))
                    r[3,n_try[Id]:NUMPOINTS,jj,m] = interp1d(n_try[Id:],numpy.squeeze(r[3,list(n_try[Id:]),jj,m]))(numpy.arange(n_try[Id],NUMPOINTS,1))
            
            # Calculate the numerical derivative of the displacement-stress vectors
            # Note that only dr1 and dr2 are needed later. dr3 and dr4 are not calculated.
            temp, dr[:,:,jj,m] = numpy.gradient(numpy.squeeze(r[0:2,:,jj,m]),1,numpy.gradient(z[:,jj])[0])
#            dr[:,:,jj,m] = dr[:,:,jj,m]*2   # how the gradient works in Python ? Why need *2 ?
    return(z,r,dr)
    
    
#%% 

def updown(thk,cvp,cvs,om,k,z,layer):
    
    """
    This function calculates the up-going and down-going matrices for the P-SV case
    Note that the function psv also calculates up-going and down-going matrices
    (called du) which are optimized for use in calculating the modified R/T
    coefficients. The matrices calculated in this function are used in calculating
    the displacement-stress vectors.
    """
    
    cvs2 = cvs[layer]**2 
    cvp2 = cvp[layer]**2
    depth = numpy.append(0, numpy.cumsum(thk))

    lamd = numpy.zeros([2,2], dtype = complex)
    lamu = numpy.zeros([2,2], dtype = complex)

    k2 = k**2 
    om2 = om**2
   
    ks2 = om2/cvs2
    nus = (k2-ks2+0j)**0.5
    if numpy.imag(-1j*nus) > 0:
        nus = -nus
           
   
    kp2 = om2/cvp2
    nup = (k2-kp2+0j)**0.5
    if numpy.imag(-1j*nup) > 0:
        nup = -nup
   
    lamd[0,0] = numpy.exp(-nup*(z-depth[layer]))
    lamd[1,1] = numpy.exp(-nus*(z-depth[layer]))

    if (layer+1) <= len(thk):
        lamu[0,0] = numpy.exp(-nup*(depth[layer+1]-z))
        lamu[1,1] = numpy.exp(-nus*(depth[layer+1]-z))

    
    return(lamd,lamu)
#%%    
def partial(freq,vr,z,r,dr,thk,dns,cvs,cvp):
    """
    This function calculates the partial derivatives of the phase velocity
    with respect to the shear and compression wave velocities for each mode
    at each frequency. Two forms of the partial derivatives are returned:
    1) the partial derivatives at individual depths (zdvrvs and zdvrvs)
    2) the partial derivatives integrated over each layer (dvrvs and dvrvp)

    """
    
    global NUMPOINTS, MAXROOT
    
    # Initialize vectors for material properties
    vs = numpy.zeros(NUMPOINTS)
    vp = numpy.zeros(NUMPOINTS)
    lame = numpy.zeros(NUMPOINTS)
    shear = numpy.zeros(NUMPOINTS)
    rho = numpy.zeros(NUMPOINTS)
    
    # Initialize matrices for the energy integrals, group velocity, and partial derivatives
    I1 = numpy.zeros([len(freq),MAXROOT], dtype = complex)
    I2 = numpy.zeros([len(freq),MAXROOT], dtype = complex)
    I3 = numpy.zeros([len(freq),MAXROOT], dtype = complex)
    U = numpy.zeros([len(freq),MAXROOT], dtype = complex)
    zdvrvs = numpy.zeros([NUMPOINTS,len(freq),MAXROOT], dtype = complex)
    zdvrvp = numpy.zeros([NUMPOINTS,len(freq),MAXROOT], dtype = complex)
    zdvrrho= numpy.zeros([NUMPOINTS,len(freq),MAXROOT], dtype = complex)
    dvrvs = numpy.zeros([len(dns),len(freq),MAXROOT], dtype = complex)
    dvrvp = numpy.zeros([NUMPOINTS,len(freq),MAXROOT], dtype = complex)
    dvrrho= numpy.zeros([NUMPOINTS,len(freq),MAXROOT], dtype = complex)
    
    # Define the vector of circular frequencies
    om = 2*numpy.pi*freq
    
    # Loop through the frequencies
    for jj in range(len(freq)):
        
        # Loop through the vector of depths to assign vectors of material properties
        for n in range(NUMPOINTS):
            
            # Determine the layer corresponding to the current depth
            index1 = numpy.where(z[n,jj] <= numpy.append(numpy.cumsum(thk), z[NUMPOINTS-1,jj]))[0]
            layer = index1[0]
            
            # Assign layer properties to vectors
            vs[n] = cvs[layer]
            vp[n] = cvp[layer]
            shear[n] = dns[layer]*cvs[layer]*cvs[layer]
            lame[n] = dns[layer]*cvp[layer]*cvp[layer] - 2*shear[n]
            rho[n] = dns[layer]
            
        # Loop through the modes at each frequency
        index2 = numpy.where(vr[jj,:])[0]
        
        for m in range(len(index2)):     
            # Calculate the wavenumber
            k = om[jj]/vr[jj,index2[m]]
      
            # Assign the displacement vectors and their derivatives to local variables
            r1 = numpy.squeeze(r[0,:,jj,m])
            r2 = numpy.squeeze(r[1,:,jj,m])
            dr1 = numpy.squeeze(dr[0,:,jj,m])
            dr2 = numpy.squeeze(dr[1,:,jj,m])
            
            # Calculate the first energy integral
            integrand = rho*(r1**2 + r2**2)
            I1[jj,m] = 0.5*numpy.trapz(integrand,z[:,jj])
            
            # Calculate the second energy integral
            integrand = (lame + 2*shear)*(r1**2) + shear*(r2**2)
            I2[jj,m] = 0.5*numpy.trapz(integrand,z[:,jj])
      
            # Calculate the third energy integral
            integrand = lame*r1*dr2 - shear*r2*dr1
            I3[jj,m] = numpy.trapz(integrand,z[:,jj])
            
            #Calculate the group velocity
            U[jj,m] = (2*k*I2[jj,m] + I3[jj,m])/(2*om[jj]*I1[jj,m])
            
            # Calculate the partial derivatives at each individual depth
            zdvrvs[:,jj,m] = rho*vs*((k*r2 - dr1)**2 - 4*k*r1*dr2)/(2*k**2*U[jj,m]*I1[jj,m])
            zdvrvp[:,jj,m] = rho*vp*((k*r1 + dr2)**2)/(2*k**2*U[jj,m]*I1[jj,m])
            zdvrrho[:,jj,m] = ((vp**2-2*vs**2)*(k*r1 + dr2)**2 + vs**2*( 2*k**2*r1**2+2*dr2**2+(k*r2-dr1)**2 )
                                -om[jj]**2*(r1**2+r2**2) )/(4*k**2*U[jj,m]*I1[jj,m])
            
            # add the layer boundary points
            znew = numpy.append(z[:,jj], numpy.cumsum(thk), axis = 0) 
            znew = numpy.unique(znew)
            dvsnew  = interp1d(z[:,jj], numpy.squeeze(zdvrvs[:,jj,m]))(znew)
            dvpnew  = interp1d(z[:,jj], numpy.squeeze(zdvrvp[:,jj,m]))(znew)
            drhonew = interp1d(z[:,jj], numpy.squeeze(zdvrrho[:,jj,m]))(znew)
            
            # Calculate the partial derivatives for each layer by integrating over the thickness of the layer
            depth = numpy.append(numpy.append(0, numpy.cumsum(thk)),z[NUMPOINTS-1,jj])
            
            for n in range(len(dns)):
                
                index3 = numpy.where((znew >= depth[n]) * (znew <= depth[n+1]))[0]
                if len(index3) < 5 :
                    print(['Partial derivatives at '+str(freq[jj])+' Hz for Layer '+str(n)+' may be incorrect.'])
                
                dvrvs[n,jj,m]  = numpy.trapz(dvsnew[index3],znew[index3])
                dvrvp[n,jj,m]  = numpy.trapz(dvpnew[index3],znew[index3])
                dvrrho[n,jj,m] = numpy.trapz(drhonew[index3],znew[index3])
                
                
    
    return(U,zdvrvs,zdvrvp,zdvrrho,dvrvs,dvrvp,dvrrho)
    
    
    
    
    
    
    
    
    
    
    
    
    
    