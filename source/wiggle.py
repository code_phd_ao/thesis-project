# -*- coding: utf-8 -*-
"""
Created on Sat Oct 31 16:39:40 2015

@author: mathieu
"""
from pylab import *  

def wiggle(t, sig , ax, origin=0, polarity=True, fill=True):
 
    #from matplotlib import pyplot as plt 
    
    if polarity :
        posFill='black'
        negFill=None
        lineColor='black'
    else:
        posFill=None
        negFill='black'
        lineColor='black'   
    # Plot
    #ax=None
    #ax = gca()
    #ax = axis
    # hold(True)
    if fill :
        if posFill is not None: 
            ax.fill_between(t, sig, origin,
                where=sig > origin,
                facecolor=posFill)
        if negFill is not None:
            ax.fill_between(t, sig, origin,
                where=sig < origin,
                facecolor=negFill)
    if lineColor is not None:
        ax.plot(t, sig, color=lineColor)


def subwiggle(x, t , ax, origin=0, polarity=True, fill=True, lineColor = 'black'):
 
    #from matplotlib import pyplot as plt 
    
    if polarity :
        posFill='black'
        negFill=None
    else:
        posFill=None
        negFill='black'
         
    # Plot
    #ax=None
    #ax = gca()
    #ax = axis
    # hold(True)
    if fill :
        if posFill is not None: 
            ax.fill_betweenx(t, x, origin,
                where=x > origin,
                facecolor=posFill)
        if negFill is not None:
            ax.fill_betweenx(t, x, origin,
                where=x < origin,
                facecolor=negFill)
    if lineColor is not None:
        ax.plot(x, t, color=lineColor)

def wiggles(data , t, xr, ax, overlap=0, polarity=True, color=False, lineColor = 'black', fill=True):

    ny,nx = data.shape
    
    #Normalise
    dx=xr[1]-xr[0]
    data = data / abs(data.max(axis=0))
    data=data*dx/2
    #data=data*overlap*dx
    #data=clip(data, -dx/2, dx/2)
    #data=clip(data, -dx, dx)
    
    #ax = gca()
  #  figure()
    if color :
        #ax.pcolor(xr,t,-data,cmap='seismic')
        #ax.contourf(xr,t,-data,100,cmap='seismic')
        ax.contourf(xr,t,data,100,cmap='seismic')
	
    for i in range(0,nx):
        subwiggle(data[:,i] + xr[i], t, ax, xr[i], polarity,fill, lineColor)
        
    ax.set_xlim(min(xr)-dx,max(xr)+dx)
    ax.set_ylim(t[0], t[-1])
    ax.invert_yaxis()
    ax.set_ylabel('Time [s]')
    ax.set_xlabel('Location [m]') 
